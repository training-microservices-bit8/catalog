package com.artivisi.training.microservices.catalog.dao;


import com.artivisi.training.microservices.catalog.entity.Produk;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProdukDao extends PagingAndSortingRepository<Produk, String> {

}
