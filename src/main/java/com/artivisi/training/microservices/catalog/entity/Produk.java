package com.artivisi.training.microservices.catalog.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data @Entity
public class Produk {
    @Id @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private String id;

    @NotEmpty @Size(min = 3, max = 255)
    private String kode;

    @NotEmpty @Size(min = 3, max = 255)
    private String nama;

    @NotNull @Min(10000)
    private BigDecimal harga;
}

