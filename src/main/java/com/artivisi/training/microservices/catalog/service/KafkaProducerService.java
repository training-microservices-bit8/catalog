package com.artivisi.training.microservices.catalog.service;

import com.artivisi.training.microservices.catalog.entity.Produk;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class KafkaProducerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerService.class);

    @Autowired private ObjectMapper objectMapper;
    @Autowired private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${topic.notification}")
    private String topicNotifikasi;

    public void kirimNotifikasiProdukBaru(Produk produk){
        try{
            LOGGER.debug("Menerima message produk baruL {}", produk);
            String msg = objectMapper.writeValueAsString(produk);
            LOGGER.debug("Konversi json menjadi object Produk: {}", msg);
            kafkaTemplate.send(topicNotifikasi, msg);
        } catch(IOException e){
            LOGGER.error(e.getMessage(), e);
        }
    }
}
